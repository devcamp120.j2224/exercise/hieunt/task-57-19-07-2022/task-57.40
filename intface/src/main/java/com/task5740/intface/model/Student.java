package com.task5740.intface.model;

import java.util.ArrayList;

public class Student extends Person implements ISchool {
	private int studentId;
	public boolean isSchool() {
		return true;
	}
	private ArrayList<Subject> listSubject;
	// khởi tạo ko có studentId và listSubject
	public Student(int age, String gender,
	String name, Address address,
	ArrayList<Animal> listPet) {
		super(age, gender, name, address, listPet);
	}
	// khởi tạo đầy đủ tham số
	public Student(int age, String gender,
	String name, Address address,
	ArrayList<Animal> listPet, int studentId,
	ArrayList<Subject> listSubject) {
		super(age, gender, name, address, listPet);
		this.studentId = studentId;
		this.listSubject = listSubject;
	}
	@Override
	public void eat() {
		System.out.println("Student is eating");
	}
	public void doHomework() {
		System.out.println("Student do homework");
	}
	public int getStudentId() {
		return studentId;
	}
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}
	public ArrayList<Subject> getListSubject() {
		return listSubject;
	}
	public void setListSubject(ArrayList<Subject> listSubject) {
		this.listSubject = listSubject;
	}
}