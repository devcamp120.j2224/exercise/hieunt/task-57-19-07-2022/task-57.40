package com.task5740.intface.model;

import java.util.ArrayList;

public class Professor extends Person implements ISchool {
	private int salary;
	public boolean isSchool() {
		return true;
	}
	// khởi tạo thiếu salary
	public Professor(int age, String gender,
	String name, Address address,
	ArrayList<Animal> listPet) {
		super(age, gender, name, address, listPet);
		this.salary = 6000;
	}
	// khởi tạo đầy đủ tham số
	public Professor(int age, String gender,
	String name, Address address,
	ArrayList<Animal> listPet, int salary) {
		super(age, gender, name, address, listPet);
		this.salary = salary;
	}
	@Override
	public void eat() {
		System.out.println("Professor is eating");
	}
	public void teaching() {
		System.out.println("Professor is teaching");
	}
	// getter setter
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
}
