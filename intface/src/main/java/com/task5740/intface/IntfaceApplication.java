package com.task5740.intface;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IntfaceApplication {

	public static void main(String[] args) {
		SpringApplication.run(IntfaceApplication.class, args);
	}

}
